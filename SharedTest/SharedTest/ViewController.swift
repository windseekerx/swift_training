//
//  ViewController.swift
//  SharedTest
//
//  Created by WenxuanWang on 8/31/16.
//  Copyright © 2016 NTTDATA.INC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let str:String = "string to test file system"
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let writePath = NSURL(fileURLWithPath: documentsPath).appendingPathComponent("test.txt")
        let fileManager  = FileManager.default
        if (fileManager.fileExists(atPath: writePath!.absoluteString)) {
            
        }else{
            do { try str.write(to: writePath!, atomically:true, encoding: .utf8)}catch let error as Error{ print(error)}
            
            
        
        }
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

