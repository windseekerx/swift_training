//
//  ViewController.swift
//  TestMVVM
//
//  Created by WenxuanWang on 8/30/16.
//  Copyright © 2016 NTTDATA.INC. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var mTableView:UITableView?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mTableView = UITableView(frame: CGRect(x:0,y:0,width:self.view.frame.width,height:self.view.frame.height),style:UITableViewStyle.plain)
        self.view.addSubview(self.mTableView!)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }


}

